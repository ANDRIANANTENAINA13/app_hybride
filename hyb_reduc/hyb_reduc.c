#include "hyb_reduc.h"

#include <mpi.h>
#include <stdlib.h>

void shared_reduc_init(shared_reduc_t *sh_red, int nthreads, int nvals)
{
    /* A COMPLETER */
	sh_red->nvals=nvals;
	sh_red->nb_thread=nthreads;

	sh_red->sh_master=0;
	sh_red->sh_fin=0;

	/*Initialiser les valeur a reduire a 0 et d'allouer dynamiquement de la memoire*/
	sh_red->red_val=calloc(sizeof(double)*nvals);

	sh_red->reduc_mutex=malloc(sizeof(pthread_mutex_t));
	pthread_mutex_init(sh_red->reduc_mutex, NULL);

	sh_red->reduc_barrier=malloc(sizeof(pthread_barrier_t));
	pthread_barrier_init(sh_red->reduc_barrier, NULL, nthreads);

}

void shared_reduc_destroy(shared_reduc_t *sh_red)
{
    /* A COMPLETER */
		free(sh_red->red_val);

		pthread_barrier_destroy(sh_red->reduc_mutex);
		free(sh_red->reduc_mutex);

		pthread_barrier_destroy(sh_red->reduc_barrier);
		free(sh_red->reduc_barrier);
}

/*
 * Reduction  hybride MPI/pthread
 * in  : tableau des valeurs a reduire (de dimension sh_red->nvals)
 * out : tableau des valeurs reduites  (de dimension sh_red->nvals)
 */
void hyb_reduc_sum(double *in, double *out, shared_reduc_t *sh_red)
{
    /* A COMPLETER */
}


